package petproject

object DataFieldNames {
    const val ID_FIELD_NAME = "id"
    const val FIRST_NAME_FIELD_NAME = "firstName"
    const val LAST_NAME_FIELD_NAME = "lastName"
    const val BIRTH_DATE_FIELD_NAME = "birthDate"
    const val EMAIL_FIELD_NAME = "email"
}
