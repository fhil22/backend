package petproject.repository

import javax.persistence.EntityManager
import javax.persistence.TypedQuery
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Root
import javax.persistence.criteria.Subquery

abstract class RepositoryHelper {

    protected abstract var entityManager: EntityManager

    /**
     * Method that create [TypedQuery] with query for what we execute code that implement in [block]
     * with return type [T] and from clause [R].
     */
    protected inline fun <reified T, reified R> query(
        block: CriteriaQuery<T>.(cb: CriteriaBuilder, root: Root<R>) -> CriteriaQuery<T>
    ): TypedQuery<T> {
        val cb = entityManager.criteriaBuilder
        val query = cb.createQuery(T::class.java)
        return entityManager.createQuery(query.block(cb, query.from(R::class.java)))
    }

    /**
     * Method that create [Subquery] with return type [T] from [R] for [query] with callback in [block]
     */
    protected inline fun <reified T, reified R> CriteriaQuery<*>.subQuery(
        block: Subquery<T>.(subQueryRoot: Root<R>) -> Subquery<T>
    ): Subquery<T> = subquery(T::class.java).apply { block(from(R::class.java)) }
}