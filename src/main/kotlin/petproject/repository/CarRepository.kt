package petproject.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import petproject.model.Car

@Repository
interface CarRepository : JpaRepository<Car, Long>, CarRepositoryCustom {

    @Transactional
    @Modifying
    @Query("UPDATE Car AS c SET c.hidden = :hidden WHERE c.id = :id")
    fun hideCar(@Param("hidden") hidden: Boolean, @Param("id") id: Long)

    fun findCarById(id: Long): Car

}