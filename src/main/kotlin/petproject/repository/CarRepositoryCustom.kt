package petproject.repository

import petproject.dto.CarDto
import petproject.dto.SortableDto

interface CarRepositoryCustom {

    fun search(sortable: SortableDto?): List<CarDto>
}