package petproject.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import petproject.model.InsuranceComplex

@Repository
interface InsuranceComplexRepository : JpaRepository<InsuranceComplex, Long>