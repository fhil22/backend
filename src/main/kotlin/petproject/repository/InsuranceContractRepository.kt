package petproject.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import petproject.model.InsuranceContract

@Repository
interface InsuranceContractRepository : JpaRepository<InsuranceContract, Long>