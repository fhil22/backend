package petproject.repository.impl

import petproject.dto.CarDto
import petproject.dto.SortDirection
import petproject.dto.SortDirection.DESC
import petproject.dto.SortableDto
import petproject.model.Car
import petproject.repository.CarRepositoryCustom
import petproject.repository.RepositoryHelper
import java.time.LocalDateTime
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

class CarRepositoryCustomImpl : CarRepositoryCustom, RepositoryHelper() {

    @PersistenceContext
    override lateinit var entityManager: EntityManager

    override fun search(sortable: SortableDto?): List<CarDto> =
        query<CarDto, Car> { cb, root ->

            multiselect(
                root.get<Long>(id),
                root.get<String>(uuid),
                root.get<LocalDateTime>(createdAt),
                root.get<LocalDateTime>(updatedAt),
                root.get<String>(color),
                root.get<Int>(engineCapacity),
                root.get<Int>(yearOfManufacture),
                root.get<Int>(weight)
            )
            where(cb.equal(root.get<Boolean>(hidden), false))

            if (sortable != null && sortable.sortBy == DESC) orderBy(cb.desc((root.get<String>(sortable.orderBy))))
            else if (sortable != null && sortable.sortBy == SortDirection.ASC) orderBy(cb.asc((root.get<String>(sortable.orderBy))))
            else orderBy(cb.asc(root.get<Long>(id)))

        }.resultList

    companion object {

        private const val id = "id"
        private const val uuid = "uuid"
        private const val createdAt = "createdAt"
        private const val updatedAt = "updatedAt"
        private const val color = "color"
        private const val engineCapacity = "engineCapacity"
        private const val yearOfManufacture = "yearOfManufacture"
        private const val weight = "weight"
        private const val hidden = "hidden"
        private const val clientId = "clientId"

    }
}