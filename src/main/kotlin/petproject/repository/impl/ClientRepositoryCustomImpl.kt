package petproject.repository.impl

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.StringUtils.isNotBlank
import petproject.DataFieldNames.BIRTH_DATE_FIELD_NAME
import petproject.DataFieldNames.EMAIL_FIELD_NAME
import petproject.DataFieldNames.FIRST_NAME_FIELD_NAME
import petproject.DataFieldNames.ID_FIELD_NAME
import petproject.DataFieldNames.LAST_NAME_FIELD_NAME
import petproject.dto.ClientDto
import petproject.dto.SortDirection.ASC
import petproject.dto.SortDirection.DESC
import petproject.dto.SortableDto
import petproject.model.Client
import petproject.repository.ClientRepositoryCustom
import petproject.repository.RepositoryHelper
import java.time.LocalDate
import java.time.LocalDateTime
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.Expression
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

class ClientRepositoryCustomImpl : ClientRepositoryCustom, RepositoryHelper() {

    @PersistenceContext
    override lateinit var entityManager: EntityManager

//    override fun search(sortable: SortableDto?): List<ClientDto> =
//        query<ClientDto, Client> { cb, root ->
//
//            multiselect(
//                root.get<Long>(id),
//                root.get<String>(uuid),
//                root.get<LocalDateTime>(createdAt),
//                root.get<LocalDateTime>(updatedAt),
//                root.get<String>(email),
//                root.get<String>(password),
//                root.get<LocalDate>(birthDate),
//                root.get<String>(firstName),
//                root.get<String>(lastName)
//            )
//
//            if (sortable != null && sortable.sortBy == DESC) orderBy(cb.desc((root.get<String>(sortable.orderBy))))
//            else if (sortable != null && sortable.sortBy == ASC) orderBy(cb.asc((root.get<String>(sortable.orderBy))))
//            else orderBy(cb.asc(root.get<Long>(id)))
//
//        }.resultList

    override fun search(sortable: SortableDto?, filterPath: String?): List<ClientDto> =
        query<ClientDto, Client> { cb, root ->

            multiselect(
                root.get<Long>(id),
                root.get<String>(uuid),
                root.get<LocalDateTime>(createdAt),
                root.get<LocalDateTime>(updatedAt),
                root.get<String>(email),
                root.get<String>(password),
                root.get<LocalDate>(birthDate),
                root.get<String>(firstName),
                root.get<String>(lastName)
            )

            where(cb.buildSearchPredicate(filterPath, root))

            if (sortable != null && sortable.sortBy == DESC) orderBy(cb.desc((root.get<String>(sortable.orderBy))))
            else if (sortable != null && sortable.sortBy == ASC) orderBy(cb.asc((root.get<String>(sortable.orderBy))))
            else orderBy(cb.asc(root.get<Long>(id)))

        }.resultList

    private fun CriteriaBuilder.buildSearchPredicate(filterPath: String?, root: Root<Client>) =
        getLikeFilterPredicates(filterPath, this, root.searchPaths)

    private val Root<Client>.searchPaths
        get() = listOf(
            get<String>(ID_FIELD_NAME),
            get<String>(FIRST_NAME_FIELD_NAME),
            get<String>(LAST_NAME_FIELD_NAME),
            get<String>(BIRTH_DATE_FIELD_NAME),
            get<String>(EMAIL_FIELD_NAME),
        )

   private fun getLikeFilterPredicates(
       filterString: String?,
       criteriaBuilder: CriteriaBuilder,
       names: List<Expression<String?>?>
   ): Predicate? {
        if (isNotBlank(filterString)) {
            var predicate: Predicate? = null
            for (name in names) {
                predicate = if (predicate == null) {
                    like(name, criteriaBuilder, filterString)
                } else {
                    criteriaBuilder.or(
                        predicate,
                        like(name, criteriaBuilder, filterString)
                    )
                }
            }
            return predicate
        }
        return null
    }

    fun like(path: Expression<String?>?, cb: CriteriaBuilder, query: String?): Predicate? {
        return cb.like(cb.lower(path), wrapWithLikeWildcardsInLowerCase(query), '!')
    }

    private fun wrapWithLikeWildcards(toWrap: String): String? {
        val escapedQuery = toWrap
            .replace("!", "!!")
            .replace("%", "!%")
            .replace("_", "!_")
        return "%$escapedQuery%"
    }

    private fun wrapWithLikeWildcardsInLowerCase(toWrap: String?): String? {
        return wrapWithLikeWildcards(toWrap!!.toLowerCase().trim { it <= ' ' })
    }

    companion object {

        private const val id = "id"
        private const val uuid = "uuid"
        private const val createdAt = "createdAt"
        private const val updatedAt = "updatedAt"
        private const val email = "email"
        private const val password = "password"
        private const val birthDate = "birthDate"
        private const val firstName = "firstName"
        private const val lastName = "lastName"
    }

}