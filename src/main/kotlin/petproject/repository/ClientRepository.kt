package petproject.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import petproject.model.Client

@Repository
interface ClientRepository : JpaRepository<Client, Long>, ClientRepositoryCustom {

    fun findClientById(id: Long): Client
}