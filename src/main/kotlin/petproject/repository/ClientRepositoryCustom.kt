package petproject.repository

import petproject.dto.ClientDto
import petproject.dto.SortableDto

interface ClientRepositoryCustom {

//    fun search(sortable: SortableDto?): List<ClientDto>
    fun search(sortable: SortableDto?, filterPath: String?): List<ClientDto>
}