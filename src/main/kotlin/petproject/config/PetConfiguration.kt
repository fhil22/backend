package petproject.config

import org.modelmapper.ModelMapper
import org.modelmapper.config.Configuration.AccessLevel.PRIVATE
import org.modelmapper.convention.MatchingStrategies.STRICT
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class PetConfiguration {

    @Bean
    fun modelMapper(): ModelMapper {
        val mapper = ModelMapper()
        mapper.configuration
            .setMatchingStrategy(STRICT)
            .setFieldMatchingEnabled(true)
            .setSkipNullEnabled(true).fieldAccessLevel = PRIVATE
        return mapper
    }
}