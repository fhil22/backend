package petproject.controller

import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import petproject.service.InsuranceComplexService

@RestController
@RequestMapping(value = ["/insurance-complexes"], produces = [APPLICATION_JSON_VALUE])
class InsuranceComplexController(
    private val insuranceComplexService: InsuranceComplexService
) {

    @GetMapping
    fun getAll() = insuranceComplexService.getAll()

    @GetMapping("/{id}")
    fun getById(@PathVariable id: Long) = insuranceComplexService.getById(id)

    @GetMapping("/lookup")
    fun lookUp() = insuranceComplexService.getLookup()
}