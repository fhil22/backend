package petproject.controller

import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import petproject.dto.CarDto
import petproject.dto.SortableDto
import petproject.service.CarService

@RestController
@RequestMapping(value = ["/cars"], produces = [APPLICATION_JSON_VALUE])
class CarController(
    private val carService: CarService,
) {

    @GetMapping
    fun search(@RequestBody sortable: SortableDto?) = carService.search(sortable)

    @PostMapping
    fun create(@RequestBody car: CarDto) = carService.create(car)

    @PostMapping("/sort")
    fun sort(@RequestBody sortable: SortableDto) = carService.search(sortable)

    @GetMapping("/delete/{id}")
    fun delete(@PathVariable id: Long) = carService.deleteById(id)

    @GetMapping("/info/{id}")
    fun getInfo(@PathVariable id: Long) = carService.getInfo(id)

    @GetMapping("/lookup")
    fun getLookup() = carService.getLookup()

}