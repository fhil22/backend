package petproject.controller

import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import petproject.dto.InsuranceContractCreateDto
import petproject.service.InsuranceContractService

@RestController
@RequestMapping(value = ["/insurance-contracts"], produces = [APPLICATION_JSON_VALUE])
class InsuranceContractController(
    private val insuranceContractService: InsuranceContractService
) {

    @GetMapping
    fun getAll() = insuranceContractService.getAll()

    @PostMapping
    fun create(@RequestBody dto: InsuranceContractCreateDto) = insuranceContractService.create(dto)

}