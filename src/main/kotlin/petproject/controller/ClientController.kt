package petproject.controller

import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.OK
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import petproject.dto.ClientDto
import petproject.dto.SortableDto
import petproject.service.ClientService

@RestController
@RequestMapping(value = ["/clients"], produces = [APPLICATION_JSON_VALUE])
class ClientController(
    private val clientService: ClientService,
) {

    @GetMapping
    fun search(@RequestBody sortable: SortableDto?) = clientService.search(sortable)

    @PostMapping("/sort")
    fun sort(@RequestBody sortable: SortableDto) = clientService.search(sortable)

    @PostMapping
    @ResponseStatus(CREATED)
    fun create(@RequestBody client: ClientDto) = clientService.create(client)

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    fun getOne(@PathVariable id: Long) = clientService.findClientById(id)

    @PostMapping("/{id}")
    @ResponseStatus(OK)
    fun update(@RequestBody clientDto: ClientDto) = clientService.updateExistedClient(clientDto)

    @GetMapping("/info/{id}")
    fun getInfo(@PathVariable id: Long) = clientService.getClientInfo(id)

    @GetMapping("/lookup")
    fun lookUp() = clientService.getLookup()

}