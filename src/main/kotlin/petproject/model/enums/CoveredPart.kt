package petproject.model.enums

enum class CoveredPart {
    WHEELS,
    HULL,
    WINDSCREEN,
    DOORS
}