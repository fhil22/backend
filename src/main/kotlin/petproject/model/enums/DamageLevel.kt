package petproject.model.enums

enum class DamageLevel {
    LOW,
    MEDIUM,
    HIGH
}