package petproject.model

import org.apache.commons.lang3.StringUtils.EMPTY
import petproject.dto.ClientDto
import petproject.model.BaseEntity.Companion.ALLOCATION_SIZE
import petproject.model.Client.Companion.CLIENT_TABLE_NAME
import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = CLIENT_TABLE_NAME)
@SequenceGenerator(name = "sequence", sequenceName = "seq_clients", allocationSize = ALLOCATION_SIZE)
class Client(
    var email: String = EMPTY,
    var password: String = EMPTY,
    var birthDate: LocalDate? = null,
    var firstName: String = EMPTY,
    var lastName: String = EMPTY
) : BaseEntity() {

    @OneToMany
    @JoinColumn(name = CLIENT_JOIN_COLUMN)
    var cars: MutableList<Car> = mutableListOf()

    @OneToMany(mappedBy = "client")
    var insuranceContracts: MutableList<InsuranceContract> = mutableListOf()

    fun update(updated: ClientDto) {
        firstName = updated.firstName
        lastName = updated.lastName
        birthDate = updated.birthDate
    }

    companion object {

        const val CLIENT_TABLE_NAME = "CLIENTS"
        const val CLIENT_JOIN_COLUMN = "CLIENT_ID"
    }

}