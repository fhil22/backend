package petproject.model

import petproject.model.InsuranceComplex.Companion.INSURANCE_COMPLEXES_TABLE_NAME
import petproject.model.enums.CoveredPart
import petproject.model.enums.DamageLevel
import java.math.BigDecimal
import java.time.Duration
import java.time.Duration.ZERO
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = INSURANCE_COMPLEXES_TABLE_NAME)
@SequenceGenerator(name = "sequence", sequenceName = "seq_complexes", allocationSize = BaseEntity.ALLOCATION_SIZE)
class InsuranceComplex : BaseEntity() {

    var duration: Duration = ZERO

    var compensationPercent: BigDecimal = BigDecimal(0)

    @Enumerated(EnumType.STRING)
    var damageLevel: DamageLevel = DamageLevel.LOW

    @Enumerated(EnumType.STRING)
    var coveredPart: CoveredPart = CoveredPart.WHEELS

    companion object {

        const val INSURANCE_COMPLEXES_TABLE_NAME = "INSURANCE_COMPLEXES"
    }
}