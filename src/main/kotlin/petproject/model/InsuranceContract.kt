package petproject.model

import petproject.model.InsuranceContract.Companion.INSURANCE_CONTRACTS_TABLE_NAME
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = INSURANCE_CONTRACTS_TABLE_NAME)
@SequenceGenerator(name = "sequence", sequenceName = "seq_contracts", allocationSize = BaseEntity.ALLOCATION_SIZE)
class InsuranceContract : BaseEntity() {

    @ManyToOne
    @JoinColumn(name = CLIENT_JOIN_COLUMN)
    var client: Client = Client()

    @OneToOne
    @JoinColumn(name = CAR_JOIN_COLUMN)
    var car: Car = Car()

    @ManyToMany
    @JoinTable(
        name = INSURANCE_CONTRACT_INSURANCE_COMPLEX_TABLE,
        joinColumns = [(JoinColumn(name = INSURANCE_CONTRACT_JOIN_TABLE_COLUMN))],
        inverseJoinColumns = [(JoinColumn(name = INSURANCE_COMPLEX_JOIN_TABLE_COLUMN))]
    )
    var insuranceComplexes: List<InsuranceComplex> = emptyList()

    val isHidden: Boolean
        get() = car.hidden

    companion object {

        const val INSURANCE_CONTRACTS_TABLE_NAME = "INSURANCE_CONTRACTS"
        private const val INSURANCE_CONTRACT_INSURANCE_COMPLEX_TABLE = "INSURANCE_CONTRACT_INSURANCE_COMPLEX"
        private const val CLIENT_JOIN_COLUMN = "CLIENT_ID"
        private const val CAR_JOIN_COLUMN = "CAR_ID"
        private const val INSURANCE_CONTRACT_JOIN_TABLE_COLUMN = "INSURANCE_CONTRACT_ID"
        private const val INSURANCE_COMPLEX_JOIN_TABLE_COLUMN = "INSURANCE_COMPLEX_ID"

    }
}