package petproject.model

import org.apache.commons.lang3.StringUtils.EMPTY
import petproject.model.Car.Companion.CAR_TABLE_NAME
import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = CAR_TABLE_NAME)
@SequenceGenerator(name = "sequence", sequenceName = "seq_cars", allocationSize = BaseEntity.ALLOCATION_SIZE)
class Car(
    var color: String = EMPTY,
    var engineCapacity: Int = 0,
    var yearOfManufacture: LocalDate? = null,
    var weight: Int = 0,
    var hidden: Boolean = false,
) : BaseEntity() {
//TODO
//    @ManyToOne
//    private var client: Client? = null

    companion object {

        const val CAR_TABLE_NAME = "CARS"
        const val CAR_JOIN_COLUMN = "CAR_IDS"
    }
}