package petproject.model

import petproject.util.now
import petproject.util.randomStringUUID
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.SEQUENCE
import javax.persistence.Id
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class BaseEntity : Serializable {

    @Id
    @GeneratedValue(generator = "sequence", strategy = SEQUENCE)
    val id: Long = 0

    val uuid: String = randomStringUUID()
    val createdAt: LocalDateTime? = now()
    val updatedAt: LocalDateTime? = now()

    companion object {

        const val ALLOCATION_SIZE = 1
    }
}