package petproject.service.impl

import org.springframework.stereotype.Service
import petproject.dto.InsuranceContractCreateDto
import petproject.dto.InsuranceContractDto
import petproject.model.InsuranceContract
import petproject.repository.CarRepository
import petproject.repository.ClientRepository
import petproject.repository.InsuranceComplexRepository
import petproject.repository.InsuranceContractRepository
import petproject.service.InsuranceContractService
import petproject.service.transformer.InsuranceContractTransformer

@Service
class InsuranceContractServiceImpl(
    private val clientRepository: ClientRepository,
    private val carRepository: CarRepository,
    private val insuranceComplexRepository: InsuranceComplexRepository,
    private val insuranceContractRepository: InsuranceContractRepository,
    private val insuranceContractTransformer: InsuranceContractTransformer
) : InsuranceContractService {

    override fun create(dto: InsuranceContractCreateDto): InsuranceContractDto {
        val clientRef = clientRepository.getOne(dto.clientId)
        val carRef = carRepository.getOne(dto.carId)
        val insuranceComplexesRef = dto.insuranceComplexesIds.map { insuranceComplexRepository.getOne(it) }
        val insuranceContract = InsuranceContract().apply {
            client = clientRef
            car = carRef
            insuranceComplexes = insuranceComplexesRef
        }
        return insuranceContractTransformer.transform(insuranceContractRepository.save(insuranceContract))
    }

    override fun getAll(): List<InsuranceContractDto> =
        insuranceContractRepository.findAll().map { insuranceContractTransformer.transform(it) }

}