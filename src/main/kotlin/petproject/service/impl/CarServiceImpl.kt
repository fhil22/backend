package petproject.service.impl

import org.springframework.stereotype.Service
import petproject.dto.CarDto
import petproject.dto.SortableDto
import petproject.repository.CarRepository
import petproject.repository.ClientRepository
import petproject.service.CarService
import petproject.service.transformer.CarTransformer

@Service
class CarServiceImpl(
    private var carRepository: CarRepository,
    private val clientRepository: ClientRepository,
    private var carTransformer: CarTransformer
) : CarService {

    override fun create(carDto: CarDto): CarDto {
        val client = carDto.clientId.let { clientRepository.findClientById(it!!) }
        val carToCreate = carTransformer.toEntity(carDto)
        val createdCar = carRepository.save(carToCreate)
        client.cars.add(createdCar)
        clientRepository.save(client)
        return carTransformer.toDto(createdCar)
    }

    override fun search(sortable: SortableDto?): List<CarDto> = carRepository.search(sortable)

    override fun deleteById(id: Long) {
        val car = carTransformer.toEntity(findCarById(id))
        car.hidden = true
        carRepository.hideCar(true, id)
        carRepository.save(car)
    }

    override fun findCarById(id: Long): CarDto {
        val car = carRepository.findCarById(id)
        return carTransformer.toDto(car)
    }

    override fun getLookup() = carRepository.findAll().map { carTransformer.toLookup(it) }

    override fun getInfo(id: Long): CarDto {
        return findCarById(id)
    }

}