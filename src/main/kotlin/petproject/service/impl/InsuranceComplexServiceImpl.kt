package petproject.service.impl

import org.springframework.stereotype.Service
import petproject.dto.InsuranceComplexDto
import petproject.repository.InsuranceComplexRepository
import petproject.service.InsuranceComplexService
import petproject.service.transformer.InsuranceComplexTransformer

@Service
class InsuranceComplexServiceImpl(
    private val insuranceComplexRepository: InsuranceComplexRepository,
    private val insuranceComplexTransformer: InsuranceComplexTransformer
) : InsuranceComplexService {

    override fun getAll(): List<InsuranceComplexDto> =
        insuranceComplexRepository.findAll().map { insuranceComplexTransformer.transform(it) }

    override fun getById(id: Long): InsuranceComplexDto =
        insuranceComplexTransformer.transform(insuranceComplexRepository.getOne(id))

    override fun getLookup() = insuranceComplexRepository.findAll().map { insuranceComplexTransformer.toLookup(it) }

}


