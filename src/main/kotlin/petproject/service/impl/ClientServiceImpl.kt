package petproject.service.impl

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import petproject.dto.ClientDto
import petproject.dto.LookupDto
import petproject.dto.SortableDto
import petproject.model.Client
import petproject.repository.CarRepository
import petproject.repository.ClientRepository
import petproject.service.ClientService
import petproject.service.transformer.ClientTransformer

@Service
class ClientServiceImpl(
    private var clientRepository: ClientRepository,
    private var clientTransformer: ClientTransformer,
    private var carRepository: CarRepository

) : ClientService {

    override fun create(clientDto: ClientDto): ClientDto {
        val userToCreate = clientTransformer.toEntity(clientDto)
        val createdUser = clientRepository.save(userToCreate)
        return clientTransformer.toDto(createdUser)
    }

    override fun findClientById(id: Long): Client = clientRepository.findClientById(id)

    //TODO
    override fun search(sortable: SortableDto?): List<ClientDto> = clientRepository.search(sortable,"S")

    @Transactional
    override fun updateExistedClient(clientDto: ClientDto): ClientDto {
        val existedClient = findClientById(clientDto.id)

        existedClient.update(clientDto)
        clientRepository.save(existedClient)

        return clientTransformer.toDto(existedClient)
    }

    override fun getClientInfo(id: Long): ClientDto {

        return clientTransformer.toDto(findClientById(id))
    }

    override fun getLookup(): List<LookupDto> =
        clientRepository.findAll().map { clientTransformer.toLookup(it) }

}