package petproject.service

import petproject.dto.InsuranceComplexDto
import petproject.dto.LookupDto

interface InsuranceComplexService {

    fun getAll(): List<InsuranceComplexDto>

    fun getById(id: Long): InsuranceComplexDto

    fun getLookup(): List<LookupDto>

}