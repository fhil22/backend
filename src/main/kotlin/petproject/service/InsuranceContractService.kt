package petproject.service

import petproject.dto.InsuranceContractCreateDto
import petproject.dto.InsuranceContractDto

interface InsuranceContractService {

    fun create(dto: InsuranceContractCreateDto): InsuranceContractDto

    fun getAll(): List<InsuranceContractDto>
}