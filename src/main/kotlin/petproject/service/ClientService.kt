package petproject.service

import petproject.dto.ClientDto
import petproject.dto.LookupDto
import petproject.dto.SortableDto
import petproject.model.Client

interface ClientService {

    fun findClientById(id: Long): Client

    fun create(clientDto: ClientDto): ClientDto

    fun search(sortable: SortableDto?): List<ClientDto>

    fun updateExistedClient(clientDto: ClientDto): ClientDto

    fun getClientInfo(id: Long): ClientDto

    fun getLookup(): List<LookupDto>

}