package petproject.service.transformer

import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import petproject.dto.CarDto
import petproject.dto.LookupDto
import petproject.model.Car

@Component
class CarTransformer : Transformer<Car, CarDto> {

    @Autowired
    private lateinit var modelMapper: ModelMapper

    @Override
    fun toEntity(carDto: CarDto): Car {
        return modelMapper.map(carDto, Car::class.java)
    }

    @Override
    fun toDto(Car: Car): CarDto {
        return modelMapper.map(Car, CarDto::class.java)
    }

    override fun transform(car: Car) = CarDto(
        id = car.id,
        uuid = car.uuid,
        createdAt = car.createdAt,
        updatedAt = car.updatedAt,
        weight = car.weight,
        yearOfManufacture = car.yearOfManufacture,
        engineCapacity = car.engineCapacity,
        color = car.color,
    )

    override fun transform(dto: CarDto) = Car().apply {
        color = dto.color
        weight = dto.weight
        yearOfManufacture = dto.yearOfManufacture
        engineCapacity = dto.engineCapacity
    }

    fun toLookup(car: Car) = LookupDto(
        id = car.id,
        label = "id:${car.id}, Engine Capacity:${car.engineCapacity}, Weight:${car.weight}"
    )
}