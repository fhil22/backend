package petproject.service.transformer

import petproject.dto.Dto
import java.io.Serializable

interface Transformer<E : Serializable, D : Dto> {

    fun transform(var1: E): D

    fun transform(var1: D): E
}