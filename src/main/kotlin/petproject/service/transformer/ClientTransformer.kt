package petproject.service.transformer

import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import petproject.dto.ClientDto
import petproject.dto.LookupDto
import petproject.model.Client

@Component
class ClientTransformer {

    @Autowired
    private lateinit var modelMapper: ModelMapper

    @Override
    fun toEntity(clientDto: ClientDto): Client {
        return modelMapper.map(clientDto, Client::class.java)
    }

    @Override
    fun toDto(client: Client): ClientDto {
        return modelMapper.map(client, ClientDto::class.java)
    }

    fun toLookup(client: Client) = LookupDto(
        id = client.id,
        label = "${client.firstName } ${client.lastName}"
    )
}