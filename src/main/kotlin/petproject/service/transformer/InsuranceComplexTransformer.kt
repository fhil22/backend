package petproject.service.transformer

import org.springframework.stereotype.Component
import petproject.dto.InsuranceComplexDto
import petproject.dto.LookupDto
import petproject.model.InsuranceComplex

@Component
class InsuranceComplexTransformer : Transformer<InsuranceComplex, InsuranceComplexDto> {

    override fun transform(insuranceComplex: InsuranceComplex) = InsuranceComplexDto(
        id = insuranceComplex.id,
        uuid = insuranceComplex.uuid,
        createdAt = insuranceComplex.createdAt,
        updatedAt = insuranceComplex.updatedAt,
        duration = insuranceComplex.duration,
        compensationPercent = insuranceComplex.compensationPercent,
        damageLevel = insuranceComplex.damageLevel,
        coveredPart = insuranceComplex.coveredPart
    )

    override fun transform(dto: InsuranceComplexDto) = InsuranceComplex().apply {
        duration = dto.duration
        compensationPercent = dto.compensationPercent
        damageLevel = dto.damageLevel
        coveredPart = dto.coveredPart
    }

    fun toLookup(insuranceComplex: InsuranceComplex) = LookupDto(
        id = insuranceComplex.id,
        label = insuranceComplex.coveredPart.toString()
    )

}