package petproject.service.transformer

import org.springframework.stereotype.Component
import petproject.dto.InsuranceContractClientDto
import petproject.dto.InsuranceContractDto
import petproject.model.InsuranceContract
import petproject.repository.CarRepository
import petproject.repository.ClientRepository
import petproject.repository.InsuranceComplexRepository

@Component
class InsuranceContractTransformer(
    private val clientRepository: ClientRepository,
    private val carRepository: CarRepository,
    private val insuranceComplexRepository: InsuranceComplexRepository
) : Transformer<InsuranceContract, InsuranceContractDto> {

    override fun transform(dto: InsuranceContractDto) = InsuranceContract().apply {
        client = clientRepository.getOne(dto.client.id)
        car = carRepository.getOne(dto.carId)
        insuranceComplexes = dto.insuranceComplexIds.map { insuranceComplexRepository.getOne(it) }
    }

    override fun transform(insuranceContract: InsuranceContract) = InsuranceContractDto(
        id = insuranceContract.id,
        uuid = insuranceContract.uuid,
        updatedAt = insuranceContract.updatedAt,
        createdAt = insuranceContract.createdAt,
        client = InsuranceContractClientDto.from(insuranceContract.client),
        carId = insuranceContract.car.id,
        insuranceComplexIds = insuranceContract.insuranceComplexes.map { it.id }
    )

}