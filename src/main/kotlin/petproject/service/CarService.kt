package petproject.service

import petproject.dto.CarDto
import petproject.dto.LookupDto
import petproject.dto.SortableDto

interface CarService {

    fun create(carDto: CarDto): CarDto

    fun search(sortable: SortableDto?): List<CarDto>

    fun deleteById(id: Long)

    fun findCarById(id: Long): CarDto

    fun getInfo(id: Long): CarDto

    fun getLookup(): List<LookupDto>

}