package petproject.dto

class InsuranceContractCreateDto(
    val insuranceComplexesIds: List<Long>,
    val clientId: Long,
    val carId: Long
) : Dto