package petproject.dto

import org.apache.commons.lang3.StringUtils.EMPTY
import petproject.model.Client

data class InsuranceContractClientDto(
    var id: Long = 0,
    var fullName: String = EMPTY
) : Dto {

    companion object {

        @JvmStatic
        fun from(client: Client) = InsuranceContractClientDto().apply {
            id = client.id
            fullName = "${client.firstName} ${client.lastName}"
        }
    }
}
