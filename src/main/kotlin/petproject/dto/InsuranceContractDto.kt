package petproject.dto

import java.time.LocalDateTime

data class InsuranceContractDto(
    val id: Long,
    val uuid: String,
    var createdAt: LocalDateTime?,
    var updatedAt: LocalDateTime?,
    val client: InsuranceContractClientDto,
    val carId: Long,
    val insuranceComplexIds: List<Long>
) : Dto