package petproject.dto

class LookupDto(
    val id: Long,
    val label: String
) : Dto