package petproject.dto

enum class SortDirection(val label: String) {
    ASC("asc"),
    DESC("desc")
}