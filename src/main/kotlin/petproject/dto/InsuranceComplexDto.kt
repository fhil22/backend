package petproject.dto

import petproject.model.enums.CoveredPart
import petproject.model.enums.DamageLevel
import java.math.BigDecimal
import java.time.Duration
import java.time.LocalDateTime

class InsuranceComplexDto(
    val id: Long,
    val uuid: String,
    var createdAt: LocalDateTime?,
    var updatedAt: LocalDateTime?,
    var duration: Duration,
    var compensationPercent: BigDecimal,
    var damageLevel: DamageLevel,
    var coveredPart: CoveredPart
) : Dto