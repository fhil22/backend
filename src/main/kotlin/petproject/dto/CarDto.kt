package petproject.dto

import org.apache.commons.lang3.StringUtils
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.UUID

data class CarDto(
    var id: Long = 0,
    var uuid: String = UUID.randomUUID().toString(),
    var createdAt: LocalDateTime? = LocalDateTime.now(),
    var updatedAt: LocalDateTime? = LocalDateTime.now(),
    var color: String = StringUtils.EMPTY,
    var engineCapacity: Int = 0,
    var yearOfManufacture: LocalDate? = null,
    var weight: Int = 0
) : Dto {

    var clientId: Long? = null

}