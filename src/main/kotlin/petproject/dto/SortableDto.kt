package petproject.dto

import org.apache.commons.lang3.StringUtils

data class SortableDto(
    var orderBy: String = StringUtils.EMPTY,
    var sortBy: SortDirection
) : Dto

