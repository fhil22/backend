package petproject.dto

import org.apache.commons.lang3.StringUtils
import petproject.util.now
import petproject.util.randomStringUUID
import java.time.LocalDate
import java.time.LocalDateTime
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import javax.validation.constraints.Size

data class ClientDto(
    var id: Long = 0,

    var uuid: String = randomStringUUID(),

    var createdAt: LocalDateTime? = now(),

    var updatedAt: LocalDateTime? = now(),

    @NotBlank
    @Email
    var email: String = StringUtils.EMPTY,

    @NotBlank
    var password: String = StringUtils.EMPTY,

    @NotNull
    @Past
    var birthDate: LocalDate? = null,

    @NotBlank
    @Size(min = 4)
    var firstName: String = StringUtils.EMPTY,

    @NotBlank
    @Size(min = 4)
    var lastName: String = StringUtils.EMPTY,
//TODO
//    var cars: MutableList<Car>? = null,
//
//    var insuranceContracts: MutableList<InsuranceContract>? = null

) : Dto