package petproject.util

import java.util.UUID

fun randomStringUUID() = UUID.randomUUID().toString()
