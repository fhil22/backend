package petproject.util

import java.time.LocalDateTime

fun now(): LocalDateTime {
    return LocalDateTime.now()
}