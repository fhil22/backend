package pet.service

import io.mockk.every
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import petproject.PetApplication
import petproject.dto.ClientDto
import petproject.model.Client
import petproject.service.ClientService

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [PetApplication::class])
@DataJpaTest
class ClientServiceTest {

    @Autowired
    private lateinit var clientService: ClientService

    @Autowired
    private lateinit var modelMapper: ModelMapper

    @Test
    fun givenClients_whenSearch_ThenReturnDtoList() {
        // given
        val clientOne = toDto(Client(firstName = "username1"))
        val clientTwo = toDto(Client(firstName = "username2"))

        // when
        every {
            clientService.search(null)
        } returns listOf(clientOne, clientTwo)

        // execute
        val result = clientService.search(null)

        // then

        assertThat(result[0]).isEqualTo("username1")
        assertThat(result[1]).isEqualTo("username1")

        verify { clientService.search(null) }
    }

    fun toDto(client: Client): ClientDto {
        return modelMapper.map(client, ClientDto::class.java)
    }

}