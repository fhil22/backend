package pet.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.hamcrest.Matchers
import org.junit.Test
import org.junit.runner.RunWith
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import petproject.PetApplication
import petproject.dto.CarDto
import petproject.dto.ClientDto
import petproject.model.Car
import petproject.service.CarService
import petproject.util.now
import petproject.util.randomStringUUID
import java.time.LocalDate

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [PetApplication::class])
@AutoConfigureMockMvc
class CarControllerTest {

    @MockkBean
    private lateinit var carService: CarService

    @Autowired
    private lateinit var modelMapper: ModelMapper

    @Autowired
    private lateinit var mvc: MockMvc

    @Autowired
    protected lateinit var objectMapper: ObjectMapper

    private val carDtoWithAllFieldsFilled: CarDto
        get() = CarDto().apply {
            id = 1
            uuid = randomStringUUID()
            createdAt = now()
            updatedAt = now()
            color = "RED"
            engineCapacity = 100
            yearOfManufacture = LocalDate.of(2000, 5, 10)
            weight = 400
        }

    @Test
    fun givenClients_whenFindAll_thenReturnAllUsers() {
        // given
        val carDtoOne = CarDto(id = 1L, color = "color1")
        val carDtoTwo = CarDto(id = 1L, color = "color2")

        val allCars = listOf(carDtoOne, carDtoTwo)

        // when
        every { carService.search(any()) } returns allCars

        // then
        mvc.perform(MockMvcRequestBuilders.get("/cars").contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Matchers.`is`(carDtoOne.id.toInt())))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].color", Matchers.`is`(carDtoOne.color)))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", Matchers.`is`(carDtoTwo.id.toInt())))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].color", Matchers.`is`(carDtoTwo.color)))
    }

    @Test
    fun givenClient_whenFindOne_thenReturnClient() {

    }

    fun toEntity(carDto: CarDto): Car {
        return modelMapper.map(carDto, Car::class.java)
    }

    private fun convertToMultipart(client: ClientDto) =
        MockMultipartFile(
            "client",
            "dto",
            MediaType.APPLICATION_JSON_VALUE,
            objectMapper.writeValueAsBytes(client)
        )

}