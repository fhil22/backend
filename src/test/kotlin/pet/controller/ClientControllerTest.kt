package pet.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.hamcrest.Matchers.`is`
import org.junit.Test
import org.junit.runner.RunWith
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import petproject.PetApplication
import petproject.dto.ClientDto
import petproject.model.Client
import petproject.service.ClientService
import petproject.util.now
import petproject.util.randomStringUUID
import java.time.LocalDate

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [PetApplication::class])
@AutoConfigureMockMvc
class ClientControllerTest {

    @MockkBean
    private lateinit var clientService: ClientService

    @Autowired
    private lateinit var modelMapper: ModelMapper

    @Autowired
    private lateinit var mvc: MockMvc

    @Autowired
    protected lateinit var objectMapper: ObjectMapper

    private val clientDtoWithAllFieldsFilled: ClientDto
        get() = ClientDto().apply {
            id = 1
            uuid = randomStringUUID()
            createdAt = now()
            updatedAt = now()
            email = "email@email"
            password = "password"
            birthDate = LocalDate.of(2000, 5, 10)
            firstName = "Name"
            lastName = "Surname"
        }

    @Test
    fun givenClients_whenFindAll_thenReturnAllUsers() {
        // given
        val clientDtoOne = ClientDto(id = 1L, firstName = "username1")
        val clientDtoTwo = ClientDto(id = 1L, firstName = "username2")

        val allUsers = listOf(clientDtoOne, clientDtoTwo)

        // when
        every { clientService.search(any()) } returns allUsers

        // then
        mvc.perform(MockMvcRequestBuilders.get("/clients").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$[0].id", `is`(clientDtoOne.id.toInt())))
            .andExpect(jsonPath("$[0].firstName", `is`(clientDtoOne.firstName)))
            .andExpect(jsonPath("$[1].id", `is`(clientDtoTwo.id.toInt())))
            .andExpect(jsonPath("$[1].firstName", `is`(clientDtoTwo.firstName)))
    }

    @Test
    fun givenClient_whenFindOne_thenReturnClient() {
        // given
        val clientDto = ClientDto(firstName = "firstnameDto", lastName = "lastnameDto")

        val client = toEntity(clientDto)

        // when
        every { clientService.findClientById(1L) } returns client

        // then
        mvc.perform(MockMvcRequestBuilders.get("/clients/1").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.firstName", `is`(client.firstName)))
            .andExpect(jsonPath("$.lastName", `is`(client.lastName)))

    }

    @Test
    fun givenUser_whenCreate_thenReturnNewUserDto() {
        // given
        val client = clientDtoWithAllFieldsFilled

        // when
        every { clientService.create(any()) } returns client

        val clientDto = convertToMultipart(client)

        // then
        mvc.perform(multipart("/clients").file(clientDto))
            .andExpect(status().isCreated)
            .andExpect(jsonPath("$.id", `is`(client.id)))
            .andExpect(jsonPath("$.uuid", `is`(client.uuid)))
            .andExpect(jsonPath("$.createdAt", `is`(client.createdAt)))
            .andExpect(jsonPath("$.updatedAt", `is`(client.updatedAt)))
            .andExpect(jsonPath("$.email", `is`(client.email)))
            .andExpect(jsonPath("$.password", `is`(client.password)))
            .andExpect(jsonPath("$.birthDate", `is`(client.birthDate)))
            .andExpect(jsonPath("$.firstName", `is`(client.firstName)))
            .andExpect(jsonPath("$.lastName", `is`(client.lastName)))
    }

    @Test
    fun givenUser_whenUpdate_thenReturnUpdatedUserDto() {
        // given
        val clientDto = clientDtoWithAllFieldsFilled

        // when
        every { clientService.updateExistedClient(any()) } returns clientDto

        val userData = convertToMultipart(clientDto)

        // then
        mvc.perform(multipart("/clients/1").file(userData))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.id", `is`(clientDto.id)))
            .andExpect(jsonPath("$.uuid", `is`(clientDto.uuid)))
            .andExpect(jsonPath("$.createdAt", `is`(clientDto.createdAt)))
            .andExpect(jsonPath("$.updatedAt", `is`(clientDto.updatedAt)))
            .andExpect(jsonPath("$.email", `is`(clientDto.email)))
            .andExpect(jsonPath("$.password", `is`(clientDto.password)))
            .andExpect(jsonPath("$.birthDate", `is`(clientDto.birthDate)))
            .andExpect(jsonPath("$.firstName", `is`(clientDto.firstName)))
            .andExpect(jsonPath("$.lastName", `is`(clientDto.lastName)))
    }

    fun toEntity(clientDto: ClientDto): Client {
        return modelMapper.map(clientDto, Client::class.java)
    }

    fun toDto(client: Client): ClientDto {
        return modelMapper.map(client, ClientDto::class.java)
    }

    private fun convertToMultipart(client: ClientDto) =
        MockMultipartFile(
            "client",
            "dto",
            MediaType.APPLICATION_JSON_VALUE,
            objectMapper.writeValueAsBytes(client)
        )

}