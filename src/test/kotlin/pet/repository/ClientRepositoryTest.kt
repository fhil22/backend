package pet.repository

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import petproject.PetApplication
import petproject.model.Client
import petproject.repository.ClientRepository
import java.time.LocalDate

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [PetApplication::class])
@DataJpaTest
class ClientRepositoryTest {

    @Autowired
    protected lateinit var testEntityManager: TestEntityManager

    @Autowired
    private lateinit var clientRepository: ClientRepository

    @Test
    fun givenUser_whenFindById_thenReturnUser() {
        // given
        val client = Client(
            email = "Peter",
            password = "1",
            birthDate = LocalDate.of(2000, 5, 10),
            firstName = "password",
            lastName = "1@email.com",
        )

        testEntityManager.persist(client)
        testEntityManager.flush()

        // when
        val found = clientRepository.findClientById(31)

        // then
        assertThat(found).isNotNull
        assertThat(found.email).isEqualTo(client.email)
        assertThat(found.password).isEqualTo(client.password)
        assertThat(found.birthDate).isEqualTo(client.birthDate)
        assertThat(found.firstName).isEqualTo(client.firstName)
        assertThat(found.lastName).isEqualTo(client.lastName)
    }
}